/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package single_thread

import hyperion.logadapter.*
import redis_sdk.client.api.*
import redis_sdk.client.*
import std.argopt.ArgOpt
import std.convert.Parsable
import std.os.getArgs
import std.runtime.GC
import std.sync.AtomicInt64
import std.sync.AtomicBool
import std.sync.SyncCounter
import std.sync.sleep
import std.time.Duration
import std.time.DateTime

let DEFAULT_HOST = "127.0.0.1"
let DEFAULT_PORT: UInt16 = 6379
let DEFAULT_THRED_COUNT = 100
let DEFAULT_TOTAL_REQUEST_COUNT = 10000000

let HOST_ARG = "--host"
let PORT_ARG = "--port"
let PASSSWORD_ARG = "--password"
let TOTAL_REQUEST_COUNT = "--totalRequestCount"
let LONG_ARG_LIST: Array<String> = Array<String>(["host=", "port=", "password=", "totalRequestCount="])
main(): Unit {
    // LoggerFactory.setLevel(LogLevel.TRACE)
    println(
        "Usage: single_thread/build/release/bin/main --host=127.0.0.1 --port=8090 --password=mypassword --totalRequestCount=10000000"
    )
    let args = getArgs()
    if (args.size > 0) {
        println("Command line args: ${args}")
    }

    println("Start BenchMarkClient")
    let argOpt = ArgOpt(args, "", LONG_ARG_LIST)

    var host = DEFAULT_HOST
    var port = DEFAULT_PORT
    var password: ?String = None
    var totalRequestCount = DEFAULT_TOTAL_REQUEST_COUNT
    if (let Some(argHost) <- argOpt.getArg(HOST_ARG)) {
        host = argHost
    }

    if (let Some(argPort) <- argOpt.getArg(PORT_ARG)) {
        port = UInt16.parse(argPort)
    }

    if (let Some(argPassword) <- argOpt.getArg(PASSSWORD_ARG)) {
        password = argPassword
    }

    if (let Some(argTotalRequestCount) <- argOpt.getArg(TOTAL_REQUEST_COUNT)) {
        totalRequestCount = Int64.parse(argTotalRequestCount)
    }

    println("Redis Server: ${host}:${port}")
    if (let Some(password) <- password) {
        println("Password: ${password}")
    }
    println("Total request count: ${totalRequestCount}")

    let clientConfig = RedisClientConfig()
    clientConfig.host = host
    clientConfig.port = port
    if (let Some(password) <-password) {
        clientConfig.password = password.toArray()
    }
    // 是否开启每个连接一个写线程
    clientConfig.asyncWrite = true
    // 是否通过切片方式减少数组拷贝
    clientConfig.sliceExceedBuffer = true
    // 是否将ByteBuffer池化重用
    clientConfig.usePooledBufferAllocator = true

    //redis客户端初始化
    let redisClientBuilder = RedisClientBuilder.builder(clientConfig)
    let client = redisClientBuilder.build()

    let latch = SyncCounter(1)
    let task = LoopSetGetTask(client, totalRequestCount, latch)
    spawn {
        task.run()
    }

    // 开始执行性能测试
    latch.dec()

    // statistic tps
    var lastTime = DateTime.now()
    var lastCount = 0
    while (true) {
        sleep(Duration.second * 10);
        let now = DateTime.now()
        let currentCount = LoopSetGetTask.totalInvokeCount.load()
        let delatCount = currentCount - lastCount
        let tps = delatCount * 1000 / (now.toUnixTimeStamp().toMilliseconds() -
            lastTime.toUnixTimeStamp().toMilliseconds());
        lastCount = currentCount
        lastTime = now
        println("Total count: ${currentCount}, TPS: ${tps}")

        if (totalRequestCount != -1 && (currentCount >= totalRequestCount)) {
            if (LoopSetGetTask.running.load()) {
                LoopSetGetTask.running.compareAndSwap(true, false)
            }
        }

        if (tps <= 0) {
            break;
        }
    }

    println("Stop BenchMarkClient")
    // GC后打HeapDump，查看是否有内存泄露
    GC(heavy: false)

    sleep(Duration.minute * 1)
}

public class LoopSetGetTask {
    static let totalInvokeCount = AtomicInt64(0)

    static let requestIdGenerator = AtomicInt64(300)

    static let running = AtomicBool(true)

    private let redisClient: UnifiedRedisClient

    private let executeNum: Int64

    private let latch: SyncCounter

    public init(redisClient: UnifiedRedisClient, executeNum: Int64, latch: SyncCounter) {
        this.redisClient = redisClient
        this.executeNum = executeNum
        this.latch = latch
    }

    public func run() {
        // 等待主线程放行
        latch.waitUntilZero()

        var i = 0
        while (running.load() && (executeNum == -1 || i < executeNum)) {
            // 发送消息，并收取对应的响应
            let id = requestIdGenerator.fetchAdd(1)
            var key = "foo${id}"
            try {
                redisClient.set(key, "bar${i}")
                redisClient.get(key)
                totalInvokeCount.fetchAdd(2)
                i = i + 2
            } catch (ex: Exception) {
                ex.printStackTrace()
                break
            }
        }
    }
}
