/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_client_unit_test

import std.unittest.testmacro.*

@Test
public class TransactionCommandsTest {
    private var redisClient: ?RedisClient = None<RedisClient>

    @BeforeAll
    protected func beforeAll(): Unit {
        redisClient = TestUtils.getRedisClient()
    }

    @TestCase
    public func testMulti(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        client.del("foo")
        let transaction: Transaction = client.multi()
        transaction.appendCommand(RedisCommandBuilder.sadd("foo", "a"))
        transaction.appendCommand(RedisCommandBuilder.sadd("foo", "b"))
        transaction.appendCommand(RedisCommandBuilder.scard("foo"))
        let response: ArrayList<TransactionResponse> = transaction.exec()
        @Assert(response.size,3)

        let expected: ArrayList<Int64> = ArrayList<Int64>()
        expected.append(1)
        expected.append(1)
        expected.append(2)

        for (i in 0..response.size) {
            if (let Some(item) <- (response[i].get() as Int64)) {
                @Assert(item,expected[i])
            }
        }
    }

    @TestCase
    public func testTransactionResponse(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        client.set("string", "foo")
        client.lpush("list", "foo")
        client.hset("hash", "foo", "bar")
        client.zadd("zset", 1.0, "foo")
        client.sadd("set", "foo")

        let transaction: Transaction = client.multi()
        transaction.appendCommand(RedisCommandBuilder.get("string"))
        transaction.appendCommand(RedisCommandBuilder.lpop("list"))
        transaction.appendCommand(RedisCommandBuilder.hget("hash", "foo"))
        transaction.appendCommand(RedisCommandBuilder.zrange("zset", 0, -1))
        transaction.appendCommand(RedisCommandBuilder.spop("set"))
        let response: ArrayList<TransactionResponse> = transaction.exec()
        if (let Some(str) <- (response[0].get() as String)) {
            @Assert("foo",str)
        }

        if (let Some(str) <- (response[1].get() as String)) {
            @Assert("foo",str)
        }

        if (let Some(str) <- (response[2].get() as String)) {
            @Assert("bar",str)
        }

        if (let Some(list) <- (response[3].get() as ArrayList<String>)) {
            @Assert("foo", list[0])
        }

        if (let Some(str) <- (response[4].get() as String)) {
             @Assert("foo",str)
        }
    }

    @TestCase
    public func testWatch(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        let state = client.watch("mykey", "somekey")
        @Assert("OK", state)
        let transaction = client.multi()

        let otherClient = TestUtils.buildRedisClient()
        otherClient.set("mykey", "foo")

        transaction.appendCommand(RedisCommandBuilder.set("mykey", "bar"))
        let responseList = transaction.exec()

        @Assert("foo",otherClient.get("mykey"))
    }

    @TestCase
    public func testUnwatch(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        let state = client.watch("mykey", "somekey")
        @Assert("OK", state)
        @Assert("OK", client.unwatch())
        let transaction = client.multi()

        let otherClient = TestUtils.buildRedisClient()
        otherClient.set("mykey", "foo")

        transaction.appendCommand(RedisCommandBuilder.set("mykey", "bar"))
        let responseList = transaction.exec()
        @Assert(1,responseList.size)
        let response = responseList[0]
        let status = response as String
        if (let Some(str) <- status) {
            @Assert("OK", str)
        }
    }

    @TestCase
    public func testDiscard(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        let transaction = client.multi()
        let resp = transaction.discard()
        @Assert("OK", resp)
    }

    @TestCase
    public func testCloseable(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        let transaction: Transaction = client.multi()

        transaction.appendCommand(RedisCommandBuilder.set("name", "zhangsan"))
        transaction.appendCommand(RedisCommandBuilder.get("name"))

        transaction.close()
        try {
            transaction.exec()
        } catch (e: RedisException) {
            @Assert(true)
        }
    }
}
