/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package shard_subscriber_example

import std.collection.LinkedList
import std.time.Duration
import std.time.DurationExtension
import std.sync.sleep
import hyperion.buffer.ByteBuffer
import hyperion.logadapter.*
import hyperion.transport.LengthBasedFrameCodec
import hyperion.transport.LengthBasedFrameEncoder
import hyperion.transport.LengthBasedFrameDecoder
import hyperion.transport.ProtocolCodecFilter
import hyperion.transport.ByteAndStringCodec
import hyperion.transport.ProtocolCodecFilter
import hyperion.transport.IoSession
import hyperion.transport.IoFilter
import hyperion.transport.ClientEndpointConfig
import hyperion.transport.ClientTcpEndpoint
import hyperion.threadpool.ThreadPoolConfig
import hyperion.threadpool.ThreadPoolFactory
import redis_sdk.client.commands.*
import redis_sdk.client.commands.impl.*
import redis_sdk.client.api.*
import redis_sdk.client.*

main() {
    let builder = ClusterRedisClientBuilder.builder()
    builder.addClusterHostAndPort("127.0.0.1", 7080)
    builder.addClusterHostAndPort("127.0.0.1", 7081)
    builder.addClusterHostAndPort("127.0.0.1", 7082)
    builder.readTimeout(Duration.second * 60)
    builder.writeTimeout(Duration.second * 30)
    builder.receiveBufferSize(32768)
    builder.sendBufferSize(32768)
    let clusterclient = builder.build()

    let shardSubscriberBuilder = ShardRedisSubscriberBuilder.builder()
    shardSubscriberBuilder.addClusterHostAndPort("127.0.0.1", 7080)
    shardSubscriberBuilder.addClusterHostAndPort("127.0.0.1", 7081)
    shardSubscriberBuilder.addClusterHostAndPort("127.0.0.1", 7082)
    shardSubscriberBuilder.readTimeout(Duration.second * 60)
    shardSubscriberBuilder.writeTimeout(Duration.second * 30)
    shardSubscriberBuilder.receiveBufferSize(32768)
    shardSubscriberBuilder.sendBufferSize(32768)
    let shardRedisSubscriber = shardSubscriberBuilder.build()

    let listener = PrintSSubscriberListener()
    shardRedisSubscriber.setShardSubscriberListener(listener)
    shardRedisSubscriber.ssubscribe("MyChannel")

    for (i in 1..10) {
        clusterclient.spublish("MyChannel", "Shard-Message-${i}")
    }

    sleep(Duration.second * 3)

    shardRedisSubscriber.sunsubscribe("MyChannel")
    shardRedisSubscriber.sunsubscribe()

    sleep(Duration.second * 3)
}

public class PrintSSubscriberListener <: ShardSubscriberListener {
    public func onSMessage(channel: String, message: String): Unit {
        println("Receive smesage: ${message} from channel ${channel}")
    }

    public func onSSubscribe(channel: String, subscribedChannels: Int64): Unit {
        println("SSubcribe channel ${channel},  subscribedChannels: ${subscribedChannels}")
    }

    public func onSUnsubscribe(channel: String, subscribedChannels: Int64): Unit {
        println("SUnsubscribe channel ${channel},  subscribedChannels: ${subscribedChannels}")
    }

    public func onPong(message: String): Unit {
        println("Recive pong mesage: ${message}")
    }

    public func onExceptionCaught(ex: Exception): Unit {
        println("Caught exception: " + ex.message)
        ex.printStackTrace()
    }
}
