/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.api

import std.collection.ArrayList
import hyperion.transport.Session
import hyperion.transport.TransportException
import hyperion.transport.ClientTcpEndpoint

/**
 * 只有一个Endpoint的CommandExecutor
 */
public open class DefaultCommandExecutor <: CommandExecutor {

    // 此处使用ClientTcpEndpoint而不是使用SocketConnecion，方便利用连接池自带的重建、空闲超时机制
    private let endpoint: ClientTcpEndpoint

    private let respVersionAware: RespVersionAware = DefaultRespVersionAware()

    public init(endpoint: ClientTcpEndpoint) {
        this.endpoint = endpoint
    }

    public override func getRespVersion(): ?ProtocolVersion {
        return respVersionAware.getRespVersion()
    }

    public func getRespVersionAware(): RespVersionAware {
        return this.respVersionAware
    }

    public open func getEndpoint(): ClientTcpEndpoint {
        return endpoint
    }

    public open func getEndpoint(command: RedisCommand): ClientTcpEndpoint {
        return getEndpoint()
    }

    protected open func internalClose(): Unit {
        this.endpoint.stop()
    }
}
