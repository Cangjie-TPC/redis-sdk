/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.api

public abstract class BaseCommand {
    private let commandExecutor: CommandExecutor

    public init(commandExecutor: CommandExecutor) {
        this.commandExecutor = commandExecutor
    }

    public func getCommandExecutor(): CommandExecutor {
        return this.commandExecutor
    }

    /**
     * 只发送命令，不返回响应。相关的RedisMessage响应会通过RedisCommandHandler设置到RedisCommand中，由调用方解析。
     */
    public func sendCommand(command: RedisCommand): Unit {
        commandExecutor.sendCommand(command)
    }

    public func executeCommand(command: RedisCommand): RedisMessage {
        return commandExecutor.executeCommand(command)
    }

    public func executeCommand<T>(command: RedisCommand, responseBuilder: ResponseBuilder<T>): T {
        return commandExecutor.executeCommand(command, responseBuilder)
    }

    public func executeCommand<T>(command: RedisCommand, responseBuilder: ResponseBuilder<?T>): ?T {
        return commandExecutor.executeCommand(command, responseBuilder)
    }

    public func executeCommand<T>(command: ParameterizedRedisCommand<T>): T {
        return commandExecutor.executeCommand(command)
    }

    public func executeCommand<T>(command: ParameterizedRedisCommand<?T>): ?T {
        return commandExecutor.executeCommand(command)
    }

    public func broadcastCommand(command: RedisCommand): RedisMessage {
        return commandExecutor.broadcastCommand(command)
    }

    public func broadcastCommand<T>(command: RedisCommand, responseBuilder: ResponseBuilder<T>): T {
        return commandExecutor.broadcastCommand(command, responseBuilder)
    }

    public func broadcastCommand<T>(command: RedisCommand, responseBuilder: ResponseBuilder<?T>): ?T {
        return commandExecutor.broadcastCommand(command, responseBuilder)
    }

    public func broadcastCommand<T>(command: ParameterizedRedisCommand<T>): T {
        return commandExecutor.broadcastCommand(command)
    }

    public func broadcastCommand<T>(command: ParameterizedRedisCommand<?T>): ?T {
        return commandExecutor.broadcastCommand(command)
    }

    public func onewayCommand(command: RedisCommand): Unit {
        return commandExecutor.onewayCommand(command)
    }

    public func onewayCommand(endpoint: ClientTcpEndpoint, command: RedisCommand): Unit {
        if (commandExecutor.isClosed()) {
            throw RedisException("Client is already closed")
        }

        var needCloseSession: ?Session = None<Session>
        try {
            let session = endpoint.createSession()
            needCloseSession = Some(session)
            session.writeAndFlushMessage(command)
        } catch (ex: TransportException) {
            throw RedisException(ex.message, ex)
        } finally {
            if (let Some(session) <- needCloseSession) {
                session.close()
            }
        }
    }
}
