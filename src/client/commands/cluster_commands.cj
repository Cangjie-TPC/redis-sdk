/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands

public interface ClusterCommands {
    func asking(): String

    func clusterAddSlots(slots: Array<Int64>): String

    func clusterAddSlotsRange(ranges: Array<Int64>): String

    func clusterBumpEpoch(): String

    func clusterCountFailureReports(nodeId: String): Int64

    func clusterCountKeysInSlot(slot: Int64): Int64

    func clusterDelSlots(slots: Array<Int64>): String

    func clusterDelSlotsRange(ranges: Array<Int64>): String

    func clusterFailover(): String

    func clusterFailover(failoverOption: ClusterFailoverOption): String

    func clusterFlushSlots(): String

    func clusterForget(nodeId: String): String

    func clusterGetKeysInSlot(slot: Int64, count: Int64): ArrayList<String>

    func clusterInfo(): String

    func clusterKeySlot(key: String): Int64

    func clusterLinks(): ArrayList<HashMap<String, ?Any>>

    func clusterMeet(ip: String, port: Int64): String

    func clusterMyId(): ?String

    func clusterMyShardId(): String

    func clusterNodes(): String

    func clusterReplicas(nodeId: String): ArrayList<String>

    func clusterReplicate(nodeId: String): String

    func clusterReset(): String

    func clusterReset(resetType: ClusterResetType): String

    func clusterSaveConfig(): String

    func clusterSetConfigEpoch(configEpoch: Int64): String

    func clusterSetSlotImporting(slot: Int64, nodeId: String): String

    func clusterSetSlotMigrating(slot: Int64, nodeId: String): String

    func clusterSetSlotNode(slot: Int64, nodeId: String): String

    func clusterSetSlotStable(slot: Int64): String

    func clusterShards(): ArrayList<ClusterShardInfo>

    func readonly(): String

    func readwrite(): String

    func clusterSlots(): ArrayList<Any>
}

public interface ClusterCommandsBuilder {
    func asking(): ParameterizedRedisCommand<String>

    func clusterAddSlots(slots: Array<Int64>): ParameterizedRedisCommand<String>

    func clusterAddSlotsRange(ranges: Array<Int64>): ParameterizedRedisCommand<String>

    func clusterBumpEpoch(): ParameterizedRedisCommand<String>

    func clusterCountFailureReports(nodeId: String): ParameterizedRedisCommand<Int64>

    func clusterCountKeysInSlot(slot: Int64): ParameterizedRedisCommand<Int64>

    func clusterDelSlots(slots: Array<Int64>): ParameterizedRedisCommand<String>

    func clusterDelSlotsRange(ranges: Array<Int64>): ParameterizedRedisCommand<String>

    func clusterFailover(): ParameterizedRedisCommand<String>

    func clusterFailover(failoverOption: ClusterFailoverOption): ParameterizedRedisCommand<String>

    func clusterFlushSlots(): ParameterizedRedisCommand<String>

    func clusterForget(nodeId: String): ParameterizedRedisCommand<String>

    func clusterGetKeysInSlot(slot: Int64, count: Int64): ParameterizedRedisCommand<ArrayList<String>>

    func clusterInfo(): ParameterizedRedisCommand<String>

    func clusterKeySlot(key: String): ParameterizedRedisCommand<Int64>

    func clusterLinks(): ParameterizedRedisCommand<ArrayList<HashMap<String, ?Any>>>

    func clusterMeet(ip: String, port: Int64): ParameterizedRedisCommand<String>

    func clusterMyId(): ParameterizedRedisCommand<?String>

    func clusterMyShardId(): ParameterizedRedisCommand<String>

    func clusterNodes(): ParameterizedRedisCommand<String>

    func clusterReplicas(nodeId: String): ParameterizedRedisCommand<ArrayList<String>>

    func clusterReplicate(nodeId: String): ParameterizedRedisCommand<String>

    func clusterReset(): ParameterizedRedisCommand<String>

    func clusterReset(resetType: ClusterResetType): ParameterizedRedisCommand<String>

    func clusterSaveConfig(): ParameterizedRedisCommand<String>

    func clusterSetConfigEpoch(configEpoch: Int64): ParameterizedRedisCommand<String>

    func clusterSetSlotImporting(slot: Int64, nodeId: String): ParameterizedRedisCommand<String>

    func clusterSetSlotMigrating(slot: Int64, nodeId: String): ParameterizedRedisCommand<String>

    func clusterSetSlotNode(slot: Int64, nodeId: String): ParameterizedRedisCommand<String>

    func clusterSetSlotStable(slot: Int64): ParameterizedRedisCommand<String>

    func clusterShards(): ParameterizedRedisCommand<ArrayList<ClusterShardInfo>>

    func readonly(): ParameterizedRedisCommand<String>

    func readwrite(): ParameterizedRedisCommand<String>

    func clusterSlots(): ParameterizedRedisCommand<ArrayList<Any>>
}

public enum ClusterFailoverOption <: CompositeArg {
    FORCE | TAKEOVER

    public func buildArgs(commandArgs: CommandArgs): Unit {
        match (this) {
            case FORCE => commandArgs.add("FORCE")
            case TAKEOVER => commandArgs.add("TAKEOVER")
        }
    }
}

public enum ClusterResetType <: CompositeArg {
    SOFT | HARD

    public func buildArgs(commandArgs: CommandArgs): Unit {
        match (this) {
            case SOFT => commandArgs.add("SOFT")
            case HARD => commandArgs.add("HARD")
        }
    }
}

public class ClusterShardInfo <: ToString {
    public static let SLOTS = "slots"
    public static let NODES = "nodes"

    private let slots: ArrayList<ArrayList<Int64>>
    private let nodes: ArrayList<ClusterShardNodeInfo>

    private let clusterShardInfo: HashMap<String, Any>

    public init(map: HashMap<String, Any>) {
        slots = ResponseBuilderHelper.getValueFromMap<ArrayList<ArrayList<Int64>>>(map, SLOTS)
        nodes = ResponseBuilderHelper.getValueFromMap<ArrayList<ClusterShardNodeInfo>>(map, NODES)
        clusterShardInfo = map
    }

    public func getSlots(): ArrayList<ArrayList<Int64>> {
        return slots
    }

    public func getNodes(): ArrayList<ClusterShardNodeInfo> {
        return nodes
    }

    public func getClusterShardInfo(): HashMap<String, Any> {
        return clusterShardInfo
    }

    public func toString(): String {
        let builder = StringBuilder()
        builder.append("{")
        builder.append(SLOTS)
        builder.append(": ")
        builder.append(slots)
        builder.append("\t")

        builder.append(NODES)
        builder.append(": ")
        builder.append(nodes)
        builder.append("\t")

        builder.append("}")
        return builder.toString()
    }
}

public class ClusterShardNodeInfo <: ToString {
    public static let ID = "id"
    public static let ENDPOINT = "endpoint"
    public static let IP = "ip"
    public static let HOSTNAME = "hostname"
    public static let PORT = "port"
    public static let TLS_PORT = "tls-port"
    public static let ROLE = "role"
    public static let REPLICATION_OFFSET = "replication-offset"
    public static let HEALTH = "health"

    private let id: String
    private let endpoint: String
    private let ip: String
    private let hostname: ?String
    private let port: Int64
    private let tlsPort: ?Int64
    private let role: String
    private let replicationOffset: Int64
    private let health: String

    private let clusterShardNodeInfo: HashMap<String, Any>

    public init(map: HashMap<String, Any>) {
        clusterShardNodeInfo = map
        id = ResponseBuilderHelper.getValueFromMap<String>(map, ID)
        endpoint = ResponseBuilderHelper.getValueFromMap<String>(map, ENDPOINT)
        ip = ResponseBuilderHelper.getValueFromMap<String>(map, IP)
        hostname = ResponseBuilderHelper.getNillableValueFromMap<String>(map, HOSTNAME)
        port = ResponseBuilderHelper.getValueFromMap<Int64>(map, PORT)
        tlsPort = ResponseBuilderHelper.getNillableValueFromMap<Int64>(map, TLS_PORT)
        role = ResponseBuilderHelper.getValueFromMap<String>(map, ROLE)
        replicationOffset = ResponseBuilderHelper.getValueFromMap<Int64>(map, REPLICATION_OFFSET)
        health = ResponseBuilderHelper.getValueFromMap<String>(map, HEALTH)
    }

    public func getId(): String {
        return id
    }

    public func getEndpoint(): String {
        return endpoint
    }

    public func getIp(): String {
        return ip
    }

    public func getHostname(): ?String {
        return hostname
    }

    public func getPort(): Int64 {
        return port
    }

    public func getTlsPort(): ?Int64 {
        return tlsPort
    }

    public func getRole(): String {
        return role
    }

    public func getReplicationOffset(): Int64 {
        return replicationOffset
    }

    public func getHealth(): String {
        return health
    }

    public func getClusterShardNodeInfo(): HashMap<String, Any> {
        return clusterShardNodeInfo
    }

    public func toString(): String {
        let builder = StringBuilder()
        builder.append("{")
        builder.append(ID)
        builder.append(": ")
        builder.append(id)
        builder.append("\t")

        builder.append(ENDPOINT)
        builder.append(": ")
        builder.append(endpoint)
        builder.append("\t")

        builder.append(IP)
        builder.append(": ")
        builder.append(ip)
        builder.append("\t")

        builder.append(HOSTNAME)
        builder.append(": ")
        builder.append(hostname)
        builder.append("\t")

        builder.append(PORT)
        builder.append(": ")
        builder.append(port)
        builder.append("\t")

        builder.append(TLS_PORT)
        builder.append(": ")
        builder.append(tlsPort)
        builder.append("\t")

        builder.append(ROLE)
        builder.append(": ")
        builder.append(role)
        builder.append("\t")

        builder.append(REPLICATION_OFFSET)
        builder.append(": ")
        builder.append(replicationOffset)
        builder.append("\t")

        builder.append(HEALTH)
        builder.append(": ")
        builder.append(health)
        builder.append("\t")

        builder.append("}")
        return builder.toString()
    }
}
