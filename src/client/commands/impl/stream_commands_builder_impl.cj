/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands.impl

public class StreamCommandsBuilderImpl <: StreamCommandsBuilder {
    public func xack(key: String, group: String, ids: Array<StreamEntryID>): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key).add(group)
        for (id in ids) {
            commandArgs.add(id.toString())
        }

        return ParameterizedRedisCommand<Int64>(CommandType.XACK, commandArgs, ResponseBuilderFactory.integerBuilder)
    }

    public func xadd(key: String, hash: Map<String, String>): ParameterizedRedisCommand<?StreamEntryID> {
        let commandArgs = CommandArgs().key(key).add(StreamEntryID.NEW_ENTRY.toString())
        let iterator = hash.iterator()
        while (let Some((field, value)) <- iterator.next()) {
            commandArgs.add(field).add(value)
        }

        return ParameterizedRedisCommand<?StreamEntryID>(
            CommandType.XADD,
            commandArgs,
            NillableStreamEntryIDResponseBuilder.INSTANCE
        )
    }

    public func xadd(key: String, id: StreamEntryID, hash: Map<String, String>): ParameterizedRedisCommand<?StreamEntryID> {
        let commandArgs = CommandArgs().key(key).add(id.toString())
        let iterator = hash.iterator()
        while (let Some((field, value)) <- iterator.next()) {
            commandArgs.add(field).add(value)
        }

        return ParameterizedRedisCommand<?StreamEntryID>(
            CommandType.XADD,
            commandArgs,
            NillableStreamEntryIDResponseBuilder.INSTANCE
        )
    }

    public func xadd(key: String, params: XAddParams, hash: Map<String, String>): ParameterizedRedisCommand<?StreamEntryID> {
        let commandArgs = CommandArgs().key(key)
        params.buildArgs(commandArgs)
        let iterator = hash.iterator()
        while (let Some((field, value)) <- iterator.next()) {
            commandArgs.add(field).add(value)
        }

        return ParameterizedRedisCommand<?StreamEntryID>(
            CommandType.XADD,
            commandArgs,
            NillableStreamEntryIDResponseBuilder.INSTANCE
        )
    }

    public func xautoclaim(
        key: String,
        group: String,
        consumerName: String,
        minIdleTime: Int64,
        start: StreamEntryID,
        params: XAutoClaimParams
    ): ParameterizedRedisCommand<(StreamEntryID, ArrayList<StreamEntry>)> {
        let commandArgs = CommandArgs().key(key).add(group).add(consumerName).add(minIdleTime).add(start.toString())

        return ParameterizedRedisCommand<(StreamEntryID, ArrayList<StreamEntry>)>(
            CommandType.XAUTOCLAIM,
            commandArgs,
            params,
            StreamXautoClaimResponseBuilder.INSTANCE
        )
    }

    public func xautoclaimJustId(
        key: String,
        group: String,
        consumerName: String,
        minIdleTime: Int64,
        start: StreamEntryID,
        params: XAutoClaimParams
    ): ParameterizedRedisCommand<(StreamEntryID, ArrayList<StreamEntryID>)> {
        let commandArgs = CommandArgs().key(key).add(group).add(consumerName).add(minIdleTime).add(start.toString())

        params.buildArgs(commandArgs)
        commandArgs.add(CommandKeyword.JUSTID)
        return ParameterizedRedisCommand<(StreamEntryID, ArrayList<StreamEntryID>)>(
            CommandType.XAUTOCLAIM,
            commandArgs,
            StreamXautoClaimJustidResponseBuilder.INSTANCE
        )
    }

    public func xclaim(
        key: String,
        group: String,
        consumerName: String,
        minIdleTime: Int64,
        params: XClaimParams,
        ids: Array<StreamEntryID>
    ): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key).add(group).add(consumerName).add(minIdleTime)
        for (id in ids) {
            commandArgs.add(id.toString())
        }
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XCLAIM,
            commandArgs,
            params,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xclaimJustId(
        key: String,
        group: String,
        consumerName: String,
        minIdleTime: Int64,
        params: XClaimParams,
        ids: Array<StreamEntryID>
    ): ParameterizedRedisCommand<ArrayList<StreamEntryID>> {
        let commandArgs = CommandArgs().key(key).add(group).add(consumerName).add(minIdleTime)
        for (id in ids) {
            commandArgs.add(id.toString())
        }
        params.buildArgs(commandArgs)
        commandArgs.add(CommandKeyword.JUSTID)
        return ParameterizedRedisCommand<ArrayList<StreamEntryID>>(
            CommandType.XCLAIM,
            commandArgs,
            StreamEntryIDListResponseBuilder.INSTANCE
        )
    }

    public func xdel(key: String, ids: Array<StreamEntryID>): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key)
        for (id in ids) {
            commandArgs.add(id.toString())
        }
        return ParameterizedRedisCommand<Int64>(CommandType.XDEL, commandArgs, ResponseBuilderFactory.integerBuilder)
    }

    public func xgroupCreate(key: String, groupName: String, makeStream: Bool): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.CREATE).key(key).add(groupName).add("0-0")
        if (makeStream) {
            commandArgs.add(CommandKeyword.MKSTREAM)
        }
        return ParameterizedRedisCommand<String>(CommandType.XGROUP, commandArgs, ResponseBuilderFactory.stringBuilder)
    }

    public func xgroupCreate(key: String, groupName: String, id: StreamEntryID, makeStream: Bool): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.CREATE).key(key).add(groupName).add(id.toString())
        if (makeStream) {
            commandArgs.add(CommandKeyword.MKSTREAM)
        }
        return ParameterizedRedisCommand<String>(CommandType.XGROUP, commandArgs, ResponseBuilderFactory.stringBuilder)
    }

    public func xgroupCreateConsumer(key: String, groupName: String, consumerName: String): ParameterizedRedisCommand<Bool> {
        let commandArgs = CommandArgs().add(CommandKeyword.CREATECONSUMER).key(key).add(groupName).add(consumerName)
        return ParameterizedRedisCommand<Bool>(CommandType.XGROUP, commandArgs, ResponseBuilderFactory.boolBuilder)
    }

    public func xgroupDelConsumer(key: String, groupName: String, consumerName: String): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().add(CommandKeyword.DELCONSUMER).key(key).add(groupName).add(consumerName)
        return ParameterizedRedisCommand<Int64>(CommandType.XGROUP, commandArgs, ResponseBuilderFactory.integerBuilder)
    }

    public func xgroupDestroy(key: String, groupName: String): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().add(CommandKeyword.DESTROY).key(key).add(groupName)
        return ParameterizedRedisCommand<Int64>(CommandType.XGROUP, commandArgs, ResponseBuilderFactory.integerBuilder)
    }

    public func xgroupSetID(key: String, groupName: String, id: StreamEntryID): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.SETID).key(key).add(groupName).add(id.toString())
        return ParameterizedRedisCommand<String>(CommandType.XGROUP, commandArgs, ResponseBuilderFactory.stringBuilder)
    }

    public func xinfoConsumers(key: String, group: String): ParameterizedRedisCommand<ArrayList<StreamConsumerInfo>> {
        let commandArgs = CommandArgs().add(CommandKeyword.CONSUMERS).key(key).add(group)
        return ParameterizedRedisCommand<ArrayList<StreamConsumerInfo>>(
            CommandType.XINFO,
            commandArgs,
            StreamConsumerInfoListResponseBuilder.INSTANCE
        )
    }

    public func xinfoGroups(key: String): ParameterizedRedisCommand<ArrayList<StreamGroupInfo>> {
        let commandArgs = CommandArgs().add(CommandKeyword.GROUPS).key(key)
        return ParameterizedRedisCommand<ArrayList<StreamGroupInfo>>(
            CommandType.XINFO,
            commandArgs,
            StreamGroupInfoListResponseBuilder.INSTANCE
        )
    }

    public func xinfoStream(key: String): ParameterizedRedisCommand<StreamInfo> {
        let commandArgs = CommandArgs().add(CommandKeyword.STREAM).key(key)
        return ParameterizedRedisCommand<StreamInfo>(CommandType.XINFO, commandArgs, StreamInfoResponseBuilder.INSTANCE)
    }

    public func xinfoStreamFull(key: String): ParameterizedRedisCommand<StreamFullInfo> {
        let commandArgs = CommandArgs().add(CommandKeyword.STREAM).key(key).add(CommandKeyword.FULL)
        return ParameterizedRedisCommand<StreamFullInfo>(
            CommandType.XINFO,
            commandArgs,
            StreamFullInfoResponseBuilder.INSTANCE
        )
    }

    public func xinfoStreamFull(key: String, count: Int64): ParameterizedRedisCommand<StreamFullInfo> {
        let commandArgs = CommandArgs().add(CommandKeyword.STREAM).key(key).add(CommandKeyword.FULL).add(
            CommandKeyword.COUNT).add(count)
        return ParameterizedRedisCommand<StreamFullInfo>(
            CommandType.XINFO,
            commandArgs,
            StreamFullInfoResponseBuilder.INSTANCE
        )
    }

    public func xlen(key: String): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key)
        return ParameterizedRedisCommand<Int64>(CommandType.XLEN, commandArgs, ResponseBuilderFactory.integerBuilder)
    }

    public func xpending(key: String, groupName: String): ParameterizedRedisCommand<StreamPendingSummary> {
        let commandArgs = CommandArgs().key(key).add(groupName)
        return ParameterizedRedisCommand<StreamPendingSummary>(
            CommandType.XPENDING,
            commandArgs,
            StreamPendingSummaryResponseBuilder.INSTANCE
        )
    }

    public func xpending(key: String, groupName: String, params: XPendingParams): ParameterizedRedisCommand<ArrayList<StreamPendingEntry>> {
        let commandArgs = CommandArgs().key(key).add(groupName)
        return ParameterizedRedisCommand<ArrayList<StreamPendingEntry>>(
            CommandType.XPENDING,
            commandArgs,
            params,
            StreamPendingEntryListResponseBuilder.INSTANCE
        )
    }

    public func xrange(key: String, start: ?StreamEntryID, end: ?StreamEntryID): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key)
        if (let Some(start) <- start) {
            commandArgs.add(start.toString())
        } else {
            commandArgs.add("-")
        }
        if (let Some(end) <- end) {
            commandArgs.add(end.toString())
        } else {
            commandArgs.add("+")
        }
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xrange(key: String, start: ?StreamEntryID, end: ?StreamEntryID, count: Int64): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key)

        if (let Some(start) <- start) {
            commandArgs.add(start.toString())
        } else {
            commandArgs.add("-")
        }
        if (let Some(end) <- end) {
            commandArgs.add(end.toString())
        } else {
            commandArgs.add("+")
        }

        commandArgs.add(CommandKeyword.COUNT).add(count)
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xrange(key: String, start: String, end: String): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key).add(start).add(end)
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xrange(key: String, start: String, end: String, count: Int64): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key).add(start).add(end).add(CommandKeyword.COUNT).add(count)
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xread(xreadParams: XReadParams, streams: Map<String, StreamEntryID>): ParameterizedRedisCommand<?ArrayList<(String, 
        ArrayList<StreamEntry>)>> {
        let commandArgs = CommandArgs()
        xreadParams.buildArgs(commandArgs)
        commandArgs.add(CommandKeyword.STREAMS)

        let keys = streams.keys()
        for (key in keys) {
            commandArgs.key(key)
        }

        let values = streams.values()
        for (id in values) {
            commandArgs.add(id.toString())
        }

        return ParameterizedRedisCommand<?ArrayList<(String, ArrayList<StreamEntry>)>>(
            CommandType.XREAD,
            commandArgs,
            StreamXreadResponseBuilder.INSTANCE
        )
    }

    public func xreadGroup(
        groupName: String,
        consumer: String,
        params: XReadGroupParams,
        streams: Map<String, StreamEntryID>
    ): ParameterizedRedisCommand<?ArrayList<(String, ArrayList<StreamEntry>)>> {
        let commandArgs = CommandArgs().add(CommandKeyword.GROUP).add(groupName).add(consumer)
        params.buildArgs(commandArgs)
        commandArgs.add(CommandKeyword.STREAMS)
        let keys = streams.keys()
        for (key in keys) {
            commandArgs.key(key)
        }

        let values = streams.values()
        for (id in values) {
            commandArgs.add(id.toString())
        }
        return ParameterizedRedisCommand<?ArrayList<(String, ArrayList<StreamEntry>)>>(
            CommandType.XREADGROUP,
            commandArgs,
            StreamXreadGroupResponseBuilder.INSTANCE
        )
    }

    public func xrevrange(key: String, end: ?StreamEntryID, start: ?StreamEntryID): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key)

        if (let Some(end) <- end) {
            commandArgs.add(end.toString())
        } else {
            commandArgs.add("+")
        }

        if (let Some(start) <- start) {
            commandArgs.add(start.toString())
        } else {
            commandArgs.add("-")
        }
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XREVRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xrevrange(key: String, end: ?StreamEntryID, start: ?StreamEntryID, count: Int64): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key)
        if (let Some(end) <- end) {
            commandArgs.add(end.toString())
        } else {
            commandArgs.add("+")
        }

        if (let Some(start) <- start) {
            commandArgs.add(start.toString())
        } else {
            commandArgs.add("-")
        }

        commandArgs.add(CommandKeyword.COUNT).add(count)

        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XREVRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xrevrange(key: String, end: String, start: String): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key).add(end).add(start)
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XREVRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xrevrange(key: String, end: String, start: String, count: Int64): ParameterizedRedisCommand<ArrayList<StreamEntry>> {
        let commandArgs = CommandArgs().key(key).add(end).add(start).add(CommandKeyword.COUNT).add(count)
        return ParameterizedRedisCommand<ArrayList<StreamEntry>>(
            CommandType.XREVRANGE,
            commandArgs,
            StreamEntryListResponseBuilder.INSTANCE
        )
    }

    public func xtrim(key: String, maxLen: Int64, approximate: Bool): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key).add(CommandKeyword.MAXLEN)
        if (approximate) {
            commandArgs.add("~")
        }
        commandArgs.add(maxLen)

        return ParameterizedRedisCommand<Int64>(CommandType.XTRIM, commandArgs, ResponseBuilderFactory.integerBuilder)
    }

    public func xtrim(key: String, params: XTrimParams): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key)

        return ParameterizedRedisCommand<Int64>(
            CommandType.XTRIM,
            commandArgs,
            params,
            ResponseBuilderFactory.integerBuilder
        )
    }
}
