/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands.impl

public class ClusterCommandsImpl <: BaseCommand & ClusterCommands {
    public init(commandExecutor: CommandExecutor) {
        super(commandExecutor)
    }

    public func asking(): String {
        let redisCommand = RedisCommandBuilder.asking()
        return executeCommand<String>(redisCommand)
    }

    public func clusterAddSlots(slots: Array<Int64>): String {
        let command = RedisCommandBuilder.clusterAddSlots(slots)
        return executeCommand<String>(command)
    }

    public func clusterAddSlotsRange(ranges: Array<Int64>): String {
        let command = RedisCommandBuilder.clusterAddSlotsRange(ranges)
        return executeCommand<String>(command)
    }

    public func clusterBumpEpoch(): String {
        let command = RedisCommandBuilder.clusterBumpEpoch()
        return executeCommand<String>(command)
    }

    public func clusterCountFailureReports(nodeId: String): Int64 {
        let command = RedisCommandBuilder.clusterCountFailureReports(nodeId)
        return executeCommand<Int64>(command)
    }

    public func clusterCountKeysInSlot(slot: Int64): Int64 {
        let command = RedisCommandBuilder.clusterCountKeysInSlot(slot)
        return executeCommand<Int64>(command)
    }

    public func clusterDelSlots(slots: Array<Int64>): String {
        let command = RedisCommandBuilder.clusterDelSlots(slots)
        return executeCommand<String>(command)
    }

    public func clusterDelSlotsRange(ranges: Array<Int64>): String {
        let command = RedisCommandBuilder.clusterDelSlotsRange(ranges)
        return executeCommand<String>(command)
    }

    public func clusterFailover(): String {
        let command = RedisCommandBuilder.clusterFailover()
        return executeCommand<String>(command)
    }

    public func clusterFailover(failoverOption: ClusterFailoverOption): String {
        let command = RedisCommandBuilder.clusterFailover(failoverOption)
        return executeCommand<String>(command)
    }

    public func clusterFlushSlots(): String {
        let command = RedisCommandBuilder.clusterFlushSlots()
        return executeCommand<String>(command)
    }

    public func clusterForget(nodeId: String): String {
        let command = RedisCommandBuilder.clusterForget(nodeId)
        return executeCommand<String>(command)
    }

    public func clusterGetKeysInSlot(slot: Int64, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.clusterGetKeysInSlot(slot, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func clusterInfo(): String {
        let command = RedisCommandBuilder.clusterInfo()
        return executeCommand<String>(command)
    }

    public func clusterKeySlot(key: String): Int64 {
        let command = RedisCommandBuilder.clusterKeySlot(key)
        return executeCommand<Int64>(command)
    }

    public func clusterLinks(): ArrayList<HashMap<String, ?Any>> {
        let command = RedisCommandBuilder.clusterLinks()
        return executeCommand<ArrayList<HashMap<String, ?Any>>>(command)
    }

    public func clusterMeet(ip: String, port: Int64): String {
        let redisCommand = RedisCommandBuilder.clusterMeet(ip, port)
        return executeCommand<String>(redisCommand)
    }

    public func clusterMyId(): ?String {
        let command = RedisCommandBuilder.clusterMyId()
        return executeCommand<String>(command)
    }

    public func clusterMyShardId(): String {
        let command = RedisCommandBuilder.clusterMyShardId()
        return executeCommand<String>(command)
    }

    public func clusterNodes(): String {
        let redisCommand = RedisCommandBuilder.clusterNodes()
        return executeCommand<String>(redisCommand)
    }

    public func clusterReplicas(nodeId: String): ArrayList<String> {
        let command = RedisCommandBuilder.clusterReplicas(nodeId)
        return executeCommand<ArrayList<String>>(command)
    }

    public func clusterReplicate(nodeId: String): String {
        let command = RedisCommandBuilder.clusterReplicate(nodeId)
        return executeCommand<String>(command)
    }

    public func clusterReset(): String {
        let command = RedisCommandBuilder.clusterReset()
        return executeCommand<String>(command)
    }

    public func clusterReset(resetType: ClusterResetType): String {
        let command = RedisCommandBuilder.clusterReset(resetType)
        return executeCommand<String>(command)
    }

    public func clusterSaveConfig(): String {
        let command = RedisCommandBuilder.clusterSaveConfig()
        return executeCommand<String>(command)
    }

    public func clusterSetConfigEpoch(configEpoch: Int64): String {
        let command = RedisCommandBuilder.clusterSetConfigEpoch(configEpoch)
        return executeCommand<String>(command)
    }

    public func clusterSetSlotImporting(slot: Int64, nodeId: String): String {
        let command = RedisCommandBuilder.clusterSetSlotImporting(slot, nodeId)
        return executeCommand<String>(command)
    }

    public func clusterSetSlotMigrating(slot: Int64, nodeId: String): String {
        let command = RedisCommandBuilder.clusterSetSlotMigrating(slot, nodeId)
        return executeCommand<String>(command)
    }

    public func clusterSetSlotNode(slot: Int64, nodeId: String): String {
        let command = RedisCommandBuilder.clusterSetSlotNode(slot, nodeId)
        return executeCommand<String>(command)
    }

    public func clusterSetSlotStable(slot: Int64): String {
        let command = RedisCommandBuilder.clusterSetSlotStable(slot)
        return executeCommand<String>(command)
    }

    public func clusterShards(): ArrayList<ClusterShardInfo> {
        let command = RedisCommandBuilder.clusterShards()
        return executeCommand<ArrayList<ClusterShardInfo>>(command)
    }

    public func readonly(): String {
        let redisCommand = RedisCommandBuilder.readonly()
        return executeCommand<String>(redisCommand)
    }

    public func readwrite(): String {
        let redisCommand = RedisCommandBuilder.readwrite()
        return executeCommand<String>(redisCommand)
    }

    public func clusterSlots(): ArrayList<Any> {
        let redisCommand = RedisCommandBuilder.clusterSlots()
        return executeCommand<ArrayList<Any>>(redisCommand)
    }
}
