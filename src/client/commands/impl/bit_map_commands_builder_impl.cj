/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands.impl

public class BitMapCommandsBuilderImpl <: BitMapCommandsBuilder {
    public func bitcount(key: String): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key)
        return ParameterizedRedisCommand<Int64>(
            CommandType.BITCOUNT,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
    }

    public func bitcount(key: String, start: Int64, end: Int64): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key).add(start).add(end)
        return ParameterizedRedisCommand<Int64>(
            CommandType.BITCOUNT,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
    }

    public func bitcount(key: String, start: Int64, end: Int64, option: BitCountOption): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key).add(start).add(end)
        return ParameterizedRedisCommand<Int64>(
            CommandType.BITCOUNT,
            commandArgs,
            option,
            ResponseBuilderFactory.integerBuilder
        )
    }

    public func bitfield(key: String, arguments: Array<String>): ParameterizedRedisCommand<?ArrayList<Int64>> {
        let commandArgs = CommandArgs().key(key)
        for (argument in arguments) {
            commandArgs.add(StringArg(argument))
        }
        return ParameterizedRedisCommand<?ArrayList<Int64>>(
            CommandType.BITFIELD,
            commandArgs,
            ResponseBuilderFactory.nillableListOfIntegerBuilder
        )
    }

    public func bitfieldReadonly(key: String, arguments: Array<String>): ParameterizedRedisCommand<ArrayList<Int64>> {
        let commandArgs = CommandArgs().key(key)
        for (argument in arguments) {
            commandArgs.add(StringArg(argument))
        }

        return ParameterizedRedisCommand<ArrayList<Int64>>(
            CommandType.BITFIELD_RO,
            commandArgs,
            ResponseBuilderFactory.listOfIntegerBuilder
        )
    }

    public func bitop(op: BitOP, destKey: String, srcKeys: Array<String>): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs()
        op.buildArgs(commandArgs)
        commandArgs.key(destKey).key(srcKeys)

        return ParameterizedRedisCommand<Int64>(
            CommandType.BITOP,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
    }

    public func bitpos(key: String, value: Bool): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key)
        if (value) {
            commandArgs.add(IntegerArg(1))
        } else {
            commandArgs.add(IntegerArg(0))
        }

        return ParameterizedRedisCommand<Int64>(
            CommandType.BITPOS,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
    }

    public func bitpos(key: String, value: Bool, params: BitPosParams): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key)
        if (value) {
            commandArgs.add(IntegerArg(1))
        } else {
            commandArgs.add(IntegerArg(0))
        }
        params.buildArgs(commandArgs)

        return ParameterizedRedisCommand<Int64>(
            CommandType.BITPOS,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
    }

    public func getbit(key: String, offset: Int64): ParameterizedRedisCommand<Bool> {
        let commandArgs = CommandArgs().key(key).add(offset)
        return ParameterizedRedisCommand<Bool>(
            CommandType.GETBIT,
            commandArgs,
            ResponseBuilderFactory.boolBuilder
        )
    }

    public func setbit(key: String, offset: Int64, value: Bool): ParameterizedRedisCommand<Bool> {
        let commandArgs = CommandArgs().key(key).add(offset)
        if (value) {
            commandArgs.add(IntegerArg(1))
        } else {
            commandArgs.add(IntegerArg(0))
        }
        return ParameterizedRedisCommand<Bool>(
            CommandType.SETBIT,
            commandArgs,
            ResponseBuilderFactory.boolBuilder
        )
    }
}
