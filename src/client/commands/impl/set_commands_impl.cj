/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands.impl

public class SetCommandsImpl <: BaseCommand & SetCommands {
    public init(commandExecutor: CommandExecutor) {
        super(commandExecutor)
    }

    public func sadd(key: String, member: Array<String>): Int64 {
        let command = RedisCommandBuilder.sadd(key, member)
        return executeCommand<Int64>(command)
    }

    public func scard(key: String): Int64 {
        let command = RedisCommandBuilder.scard(key)
        return executeCommand<Int64>(command)
    }

    public func sdiff(keys: Array<String>): HashSet<String> {
        let command = RedisCommandBuilder.sdiff(keys)
        return executeCommand<HashSet<String>>(command)
    }

    public func sdiffstore(destination: String, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.sdiffstore(destination, keys)
        return executeCommand<Int64>(command)
    }

    public func sinter(keys: Array<String>): HashSet<String> {
        let command = RedisCommandBuilder.sinter(keys)
        return executeCommand<HashSet<String>>(command)
    }

    public func sintercard(keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.sintercard(keys)
        return executeCommand<Int64>(command)
    }

    public func sintercard(limit: Int64, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.sintercard(limit, keys)
        return executeCommand<Int64>(command)
    }

    public func sinterstore(destination: String, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.sinterstore(destination, keys)
        return executeCommand<Int64>(command)
    }

    public func sismember(key: String, member: String): Bool {
        let command = RedisCommandBuilder.sismember(key, member)
        return executeCommand<Bool>(command)
    }

    public func smembers(key: String): HashSet<String> {
        let command = RedisCommandBuilder.smembers(key)
        return executeCommand<HashSet<String>>(command)
    }

    public func smismember(key: String, member: Array<String>): ArrayList<Bool> {
        let command = RedisCommandBuilder.smismember(key, member)
        return executeCommand<ArrayList<Bool>>(command)
    }

    public func smove(source: String, destination: String, member: String): Bool {
        let command = RedisCommandBuilder.smove(source, destination, member)
        return executeCommand<Bool>(command)
    }

    public func spop(key: String): ?String {
        let command = RedisCommandBuilder.spop(key)
        return executeCommand<String>(command)
    }

    public func spop(key: String, count: Int64): HashSet<String> {
        let command = RedisCommandBuilder.spop(key, count)
        return executeCommand<HashSet<String>>(command)
    }

    public func srandmember(key: String): ?String {
        let command = RedisCommandBuilder.srandmember(key)
        return executeCommand<String>(command)
    }

    public func srandmember(key: String, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.srandmember(key, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func srem(key: String, member: Array<String>): Int64 {
        let command = RedisCommandBuilder.srem(key, member)
        return executeCommand<Int64>(command)
    }

    public func sunion(keys: Array<String>): HashSet<String> {
        let command = RedisCommandBuilder.sunion(keys)
        return executeCommand<HashSet<String>>(command)
    }

    public func sunionstore(destination: String, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.sunionstore(destination, keys)
        return executeCommand<Int64>(command)
    }

    public func sscan(key: String, cursor: String): (String, ArrayList<String>) {
        let command = RedisCommandBuilder.sscan(key, cursor)
        return executeCommand<(String, ArrayList<String>)>(command)
    }

    public func sscan(key: String, cursor: String, params: ScanParams): (String, ArrayList<String>) {
        let command = RedisCommandBuilder.sscan(key, cursor, params)
        return executeCommand<(String, ArrayList<String>)>(command)
    }
}
