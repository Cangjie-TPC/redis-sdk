/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands

public interface FunctionCommands {
    func fcall(name: String, keys: ArrayList<String>, args: ArrayList<String>): ?Any

    func fcallReadonly(name: String, keys: ArrayList<String>, args: ArrayList<String>): ?Any

    func functionDelete(libraryName: String): String

    func functionDump(): Array<Byte>

    func functionFlush(): String

    func functionFlush(flushMode: FlushMode): String

    func functionKill(): String

    func functionList(): ArrayList<LibraryInfo>

    func functionList(libraryNamePattern: String): ArrayList<LibraryInfo>

    func functionListWithCode(): ArrayList<LibraryInfo>

    func functionListWithCode(libraryNamePattern: String): ArrayList<LibraryInfo>

    func functionLoad(functionCode: String): String

    func functionLoadReplace(functionCode: String): String

    func functionRestore(serializedValue: Array<Byte>): String

    func functionRestore(serializedValue: Array<Byte>, policy: FunctionRestorePolicy): String

    func functionStats(): FunctionStats
}

public interface FunctionCommandsBuilder {
    func fcall(script: String, keys: ArrayList<String>, args: ArrayList<String>): ParameterizedRedisCommand<?Any>

    func fcallReadonly(name: String, keys: ArrayList<String>, args: ArrayList<String>): ParameterizedRedisCommand<?Any>

    func functionDelete(libraryName: String): ParameterizedRedisCommand<String>

    func functionDump(): ParameterizedRedisCommand<Array<Byte>>

    func functionFlush(): ParameterizedRedisCommand<String>

    func functionFlush(flushMode: FlushMode): ParameterizedRedisCommand<String>

    func functionKill(): ParameterizedRedisCommand<String>

    func functionList(): ParameterizedRedisCommand<ArrayList<LibraryInfo>>

    func functionList(libraryNamePattern: String): ParameterizedRedisCommand<ArrayList<LibraryInfo>>

    func functionListWithCode(): ParameterizedRedisCommand<ArrayList<LibraryInfo>>

    func functionListWithCode(libraryNamePattern: String): ParameterizedRedisCommand<ArrayList<LibraryInfo>>

    func functionLoad(functionCode: String): ParameterizedRedisCommand<String>

    func functionLoadReplace(functionCode: String): ParameterizedRedisCommand<String>

    func functionRestore(serializedValue: Array<Byte>): ParameterizedRedisCommand<String>

    func functionRestore(serializedValue: Array<Byte>, policy: FunctionRestorePolicy): ParameterizedRedisCommand<String>

    func functionStats(): ParameterizedRedisCommand<FunctionStats>
}

public class LibraryInfo {
    private let libraryName: ?String

    private let engine: ?String

    private let functions: ArrayList<HashMap<String, ?Any>>

    private let libraryCode: ?String

    public init(libraryName: ?String, engineName: ?String, functions: ArrayList<HashMap<String, ?Any>>) {
        this(libraryName, engineName, functions, None)
    }

    public init(libraryName: ?String, engineName: ?String, functions: ArrayList<HashMap<String, ?Any>>, code: ?String) {
        this.libraryName = libraryName
        this.engine = engineName
        this.functions = functions
        this.libraryCode = code
    }

    public func getLibraryName(): ?String {
        return this.libraryName
    }

    public func getEngine(): ?String {
        return this.engine
    }

    public func getFunctions(): ArrayList<HashMap<String, ?Any>> {
        return this.functions
    }

    public func getLibraryCode(): ?String {
        return this.libraryCode
    }
}

public class FunctionStats {
    private let runningScript: HashMap<String, Any>

    private let engines: HashMap<String, HashMap<String, Any>>

    public FunctionStats(script: HashMap<String, Any>, engines: HashMap<String, HashMap<String, Any>>) {
        this.runningScript = script
        this.engines = engines
    }

    public func getRunningScript(): HashMap<String, Any> {
        return this.runningScript
    }

    public func getEngines(): HashMap<String, HashMap<String, Any>> {
        return this.engines
    }
}

public enum FunctionRestorePolicy <: ProtocolKeyword {
    FLUSH | APPEND | REPLACE

    /**
     *
     * @return byte[] encoded representation.
     */
    public func getBytes(): Array<Byte> {
        return name().toArray()
    }

    /**
     *
     * @return name of the command.
     */
    public func name(): String {
        match (this) {
            case FLUSH => "FLUSH"
            case APPEND => "APPEND"
            case REPLACE => "REPLACE"
        }
    }
}
